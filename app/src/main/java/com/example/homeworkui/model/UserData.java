package com.example.homeworkui.model;

import java.util.ArrayList;

public class UserData {
    private static ArrayList<User> users = new ArrayList<>();

    //Create an Account: Add ArrayList to store in memory;
    public static int addUser(User user){
        int rs = 0;
        if(user != null){
            users.add(user);
            rs++;
        }
        return rs;
    }
    public static void setUsers(ArrayList<User> users) {
        UserData.users = users;
    }

    public static ArrayList<User> getUsers() {
        return users;
    }
    //Check Login: If email & Password are correct;
    public static int find(String email, String pass){
        int rs = 0;
        for (User user : users) {
            if(user.getEmail().toLowerCase().equals(email.toLowerCase()) && user.getPassword().toLowerCase().equals(pass.toLowerCase()))
                rs ++;
        }
        return rs;
    }
    //Check Existing Account;
    public static int find(String email){
        int rs = 0;
        for (User user : users) {
            if(user.getEmail().toLowerCase().equals(email.toLowerCase()))
                rs ++;
        }
        return rs;
    }
    //Get specific user;
    public static User getUser(String email, String pass){
        for (User user : users) {
            if(user.getEmail().toLowerCase().equals(email.toLowerCase()) && user.getPassword().toLowerCase().equals(pass.toLowerCase()))
                return user;
        }
        return  null;
    }

}
