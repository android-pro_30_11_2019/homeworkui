package com.example.homeworkui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.homeworkui.model.User;
import com.example.homeworkui.model.UserData;
import com.google.gson.Gson;

import static com.example.homeworkui.model.UserData.find;

public class LogInActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etEmail;
    private EditText etPassword;
    private Button btnSignIn;
    private TextView tvSignUp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Sign in to application");
        getSupportActionBar().hide();

        //
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnSignIn = findViewById(R.id.btnSignIn);
        tvSignUp = findViewById(R.id.tvSignUp);

        btnSignIn.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);

        //add default login
        UserData.addUser(new User("admin","admin@gmail.com","admin"));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSignIn:
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                int login = UserData.find(email,password);
                if(login>0){
                    Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this,MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    //Convert object user to JSON by using GSON
                    Gson gson = new Gson();
                    User user = UserData.getUser(email,password);
                    String userjson = gson.toJson(user);
                    //add pass data via intent here;
                    intent.putExtra("user",userjson);
                    startActivity(intent);
                    finish();
                }else
                    Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvSignUp:
                startActivity(new Intent(LogInActivity.this,SignUpActivity.class));
                break;
        }
    }
}
