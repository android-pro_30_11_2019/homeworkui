package com.example.homeworkui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.homeworkui.model.User;
import com.example.homeworkui.model.UserData;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etFullName;
    private EditText etEmail;
    private EditText etPassword;
    private Button btnSignUp;
    private TextView tvSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        getSupportActionBar().hide();


        //Reference View
        etFullName = findViewById(R.id.etFullName);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnSignUp = findViewById(R.id.btnSignUp);
        tvSignIn = findViewById(R.id.tvSignIn);

        //Lisener
        btnSignUp.setOnClickListener(this);
        tvSignIn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSignUp:
                String fullName = etFullName.getText().toString();
                String email = etEmail.getText().toString();
                String pass = etPassword.getText().toString();
                //Check if account exist;
                if(UserData.find(email)>0){
                    Toast.makeText(this, "Your account already exists", Toast.LENGTH_SHORT).show();
                    break;
                }
                //Create an Account: will replace if we use server site api;
                if(UserData.addUser(new User(fullName,email,pass))>0){
                    Toast.makeText(this, "Your account has been created.", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            case R.id.tvSignIn:
                finish();
                break;
        }
    }
}
