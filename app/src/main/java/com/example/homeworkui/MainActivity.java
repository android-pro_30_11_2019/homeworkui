package com.example.homeworkui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.example.homeworkui.model.User;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            String userJson = bundle.getString("user");

            Gson gson = new Gson();
            User user = gson.fromJson(userJson,User.class);
            Toast.makeText(this, "user="+user.getEmail(), Toast.LENGTH_SHORT).show();
        }
    }
}
